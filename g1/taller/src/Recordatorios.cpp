#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
            // ene, feb, mar, abr, may, jun
            31, 28, 31, 30, 31, 30,
            // jul, ago, sep, oct, nov, dic
            31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
#if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
#endif

private:
    int dia_;
    int mes_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {};

int Fecha::mes(){
    return mes_;
}

int Fecha::dia(){
    return dia_;
}

ostream& operator<< (ostream& os, Fecha f){
    os << f.dia() << "/" << f.mes();
    return os;
}

void Fecha::incrementar_dia(){
    if(dia_ + 1 > dias_en_mes(mes_))
    {
        dia_ = 1;
        if(mes_ == 12)
            mes_ = 1;
        else
            mes_++;
    }
    else
        dia_++;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}
#endif

// Ejercicio 11, 12

class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario o);
    bool operator<(Horario o);
private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min){};

uint Horario::hora(){
    return hora_;
}

uint Horario::min(){
    return min_;
}

bool Horario::operator==(Horario o){
    bool igual_hora = this->hora_ == o.hora();
    bool igual_min = this->min_ == o.min();
    return igual_hora && igual_min;
}

bool Horario::operator<(Horario o){
    bool res = false;
    if(this->hora_ < o.hora())
        res = true;
    else if (this->hora_ == o.hora())
        if(this->min_ < o.min_)
            res = true;
    return res;
}

ostream& operator<<(ostream& os, Horario h){
    os << h.hora() << ":" << h.min();
    return os;
}

// Ejercicio 13

class Recordatorio{
public:
    Recordatorio(Fecha f, Horario h, string m);
    Fecha f();
    Horario h();
    string m();
    bool operator<(Recordatorio o);
private:
    Fecha f_;
    Horario h_;
    string m_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string m) : f_(f), h_(h), m_(m) {};

bool Recordatorio::operator<(Recordatorio o){
    return h_ < o.h_;
};

Fecha Recordatorio::f()
{
    return f_;
}

Horario Recordatorio::h()
{
    return h_;
}

string Recordatorio::m()
{
    return m_;
}

ostream& operator<<(ostream& os, Recordatorio r)
{
    os << r.m() << " @ " << r.f() << " " << r.h();
    return os;
}

// Ejercicio 14

class Agenda{
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio (Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha hoy_;
    list<Recordatorio> r_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial){};

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

Fecha Agenda::hoy(){
    return hoy_;
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    r_.push_back(rec);
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> res;
    for(Recordatorio rec : r_)
        if(rec.f() == hoy_)
            res.push_back(rec);
    res.sort();
    return res;
}

ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl << "=====" << endl;
    for(Recordatorio rec : a.recordatorios_de_hoy())
        os << rec << endl;
    return os;
}
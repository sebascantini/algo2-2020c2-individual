#ifndef _LISTA_ALUMNOS_H_
#define _LISTA_ALUMNOS_H_

#include <string>
#include <ostream>

using namespace std;

typedef unsigned long Nat;

class Lista {
public:

    /**
     * Constructor por defecto de la clase Lista.
     */
    Lista();

    /**
     * Constructor por copia de la clase Lista.
     */
    Lista(const Lista& l);

    /**
     * Destructor de la clase Lista.
     */
    ~Lista();

    /**
     * Operador asignacion
     * @param aCopiar
     * @return
     */
    Lista& operator=(const Lista& aCopiar);
    void agregarAdelante(const int& elem);
    void agregarAtras(const int& elem);

    /**
     * Elimina el i-ésimo elemento de la Lista.
     * @param i posición del elemento a eliminar
     */
    void eliminar(Nat i);
    int longitud() const;

    /**
     * Devuelve el elemento en la i-ésima posición de la Lista.
     * @param i posición del elemento a devolver.
     * @return referencia no modificable
     */
    const int& iesimo(Nat i) const;
    /**
     * Devuelve el elemento en la i-ésima posición de la Lista.
     * @param i posición del elemento a devolver.
     * @return referencia modificable
     */
    int& iesimo(Nat i);

    /**
     * Muestra la lista en un ostream
     * formato de salida: [a_0, a_1, a_2, ...]
     * @param o
     */
    void mostrar(ostream& o);

    /**
     * Utiliza el método mostrar(os) para sobrecargar el operador <<
     */
    friend ostream& operator<<(ostream& os, Lista& l) {
        l.mostrar(os);
        return os;
    }



private:

    struct Nodo {
        Nodo* before;
        int val;
        Nodo* next;
        Nodo(Nodo* b, int v, Nodo* n);
    };

    Nodo* iesimoNodo(Nat i);
    Nodo* first;
    Nodo* last;
};

Lista::Nodo::Nodo(Nodo* b, int v, Nodo* n) : before(b), val(v), next(n) {}

Lista::Nodo* Lista::iesimoNodo(Nat i) {
    int j = 0;
    Nodo* temp = first;
    while(j < i && i < this->longitud())
    {
        j++;
        temp = temp->next;
    }
    return temp;
}
#include "Lista.hpp"

#endif
